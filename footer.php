</main>
<footer class="ftr">
    <div class="ftr__t">
        <div class="cnt">
            <a href="/" class="ftr__logo"><img class="ftr__logo-img" src="/local/templates/cookery/assets/dist/img/logo-bottom.svg" alt="logo"></a>
        </div>
    </div>
    <div class="ftr__b">
        <div class="cnt cnt_ftr-b">
            <nav class="b-menu">
                <ul class="b-menu__lst">
                    <li class="b-menu__itm"><a href="#" class="b-menu__l">Наша ферма</a></li>
                    <li class="b-menu__itm"><a href="#" class="b-menu__l">Кулинария</a></li>
                    <li class="b-menu__itm"><a href="#" class="b-menu__l">О нас / Контакты</a></li>
                    <li class="b-menu__itm"><a href="#" class="b-menu__l">Предложение дня</a></li>
                    <li class="b-menu__itm"><a href="#" class="b-menu__l">Новинки</a></li>
                </ul>
                <ul class="b-menu__lst b-menu__lst_r">
                    <li class="b-menu__itm"><a href="#" class="b-menu__l b-menu__l_r">Пекарня/десерты</a></li>
                    <li class="b-menu__itm"><a href="#" class="b-menu__l b-menu__l_r">Овощи/Фрукты</a></li>
                    <li class="b-menu__itm"><a href="#" class="b-menu__l b-menu__l_r">Лучшее детям</a></li>
                    <li class="b-menu__itm"><a href="#" class="b-menu__l b-menu__l_r">Меню от шефа</a></li>
                    <li class="b-menu__itm"><a href="#" class="b-menu__l b-menu__l_r">Лидеры продаж</a></li>
                </ul>
                <a href="/" class="b-menu__logo"><img src="/local/templates/cookery/assets/dist/img/main-logo.svg" alt="logo" class="b-menu__logo-img"></a>
            </nav>
            <div class="ftr__data">
                <p class="cpr">© 2016–<?php echo date('Y') ?> Воронцовская кулинария. <br>Не является публичной офертой.</p>
                <nav class="soc">
                    <ul class="soc__lst">
                        <li class="soc__itm"><a href="#" class="soc__l soc__l_vk"></a></li>
                        <li class="soc__itm"><a href="#" class="soc__l soc__l_fb"></a></li>
                        <li class="soc__itm"><a href="#" class="soc__l soc__l_inst"></a></li>
                    </ul>
                </nav>
                <a href="https://www.g-lab.ru/" target="_blank" class="g-lab">Разработка сайта G-lab</a>
            </div>
        </div>
    </div>
</footer>
</div>

</body>
</html>