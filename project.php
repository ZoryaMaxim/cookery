<?php $page = 'home' ?>
<?php require_once 'header.php' ?>
    <div class="bnr">
        <div class="bnr__swiper swiper-container">
            <div class="bnr__swiper-wrap swiper-wrapper">
                <div class="bnr__slide swiper-slide"
                     style="background-image: url('/local/templates/cookery/assets/dist/img/banner1.jpg');"></div>
                <div class="bnr__slide swiper-slide"
                     style="background-image: url('/local/templates/cookery/assets/dist/img/banner1.jpg');"></div>
                <div class="bnr__slide swiper-slide"
                     style="background-image: url('/local/templates/cookery/assets/dist/img/banner1.jpg');"></div>
            </div>
            <div class="bnr__dots"></div>
        </div>
    </div>
    <div class="t-menu__cnt">
        <nav class="t-menu">
            <div class="cnt">
                <ul class="t-menu__lst">
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">Меню от шефа</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">Кулинария</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l t-menu__l_in">Наша ферма</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">Пекарня/десерты</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">Овощи/Фрукты</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">Лучшее детям</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">Лидеры продаж</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">Предложение дня</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">Новинки</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">О нас</a></li>
                </ul>

            </div>
            <div class="t-menu__line"></div>
        </nav>
    </div>
    <div class="cnt cnt_main">
        <aside class="aside">
            <nav class="l-menu">
                <ul class="l-menu__lst">
                    <li class="l-menu__itm"><a href="#" class="l-menu__l">Меню от шефа</a></li>
                    <li class="l-menu__itm"><a href="#" class="l-menu__l">Кулинария</a></li>
                    <li class="l-menu__itm l-menu__itm_parent l-menu__itm_tablet"><a href="#" class="l-menu__l">Наша
                            ферма</a>
                        <ul class="l-menu__sub">
                            <li class="l-menu__sub-itm"><a href="#" class="l-menu__sub-l">Молочные</a>
                            <li class="l-menu__sub-itm"><a href="#" class="l-menu__sub-l">Сыр</a>
                            <li class="l-menu__sub-itm"><a href="#" class="l-menu__sub-l">Мясо</a>
                        </ul>
                    </li>
                    <li class="l-menu__itm"><a href="#" class="l-menu__l">Пекарня/десерты</a></li>
                    <li class="l-menu__itm"><a href="#" class="l-menu__l">Овощи/Фрукты</a></li>
                    <li class="l-menu__itm"><a href="#" class="l-menu__l">Лучшее детям</a></li>
                    <li class="l-menu__itm"><a href="#" class="l-menu__l">Лидеры продаж</a></li>
                    <li class="l-menu__itm"><a href="#" class="l-menu__l">Предложение дня</a></li>
                    <li class="l-menu__itm"><a href="#" class="l-menu__l">Новинки</a></li>
                    <li class="l-menu__itm"><a href="#" class="l-menu__l">О нас</a></li>
                </ul>
            </nav>
        </aside>
        <div class="main__cnt">
            <div class="brdcrmb"><span class="brdcrmb__cnt" typeof="v:Breadcrumb"><a class="brdcrmb__itm" href="/"
                                                                                     rel="v:url" property="v:title">Наша ферма</a><span
                            class="brdcrmb__itm brdcrmb__itm_last">Молочные</span></span></div>
            <div class="items">
                <div class="items__list">
                    <div class="items__itm" data-price="390.00" data-value="200" data-units="гр.">
                        <div class="items__small">
                            <figure class="items__image">
                                <a href="#" class="items__image-wrap"><img
                                            src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                            data-src="<?php echo $path, 'dist/img/itm-img.jpg' ?>"
                                            alt="Молоко пастеризованное 7%"
                                            class="items__thumb lazyload"></a>
                                <figcaption class="items__ttl"><a href="#" class="items__l">Молоко пастеризованное
                                        7%</a></figcaption>
                            </figure>
                            <div class="items__desc">Фермерское коровье молоко домашнее. Жирность 7,5%. Lorem ipsum
                                dolor sit amet, consectetur adipisicing elit. Cupiditate debitis eveniet inventore,
                                maiores nihil quisquam?
                            </div>
                            <div class="items__row">
                                <div class="items__left">
                                    <div class="items__price-sale">390.00</div>
                                    <div class="items__price">390.00</div>
                                    <div class="items__value">за 200 гр.</div>
                                </div>
                                <div class="items__right">
                                    <div class="quantity">
                                        <a href="#"
                                           class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                        <div class="quantity__value">200 гр.</div>
                                        <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                        <input type="text" class="bx_col_input quantity__count" name="quantity"
                                               value="1">
                                    </div>
                                    <a href="#" class="cart-btn">
                                        <span class="cart-btn__cnt" data-add="Добавлено">В корзину</span>
                                        <span class="cart-btn__progress"><span class="cart-btn__inner"></span></span>
                                    </a>
                                </div>
                            </div>
                            <a href="#" class="items__mark"><span
                                        class="items__mark-value">Хит</span></a>
                        </div>
                        <div class="modal-item">
                            <div class="modal-item__cnt">
                                <div class="modal-item__inner">
                                    <div class="modal-item__close"></div>
                                    <div class="modal-item__wrp">
                                        <figure class="modal-item__image">
                                        <span class="modal-item__image-wrap"><img
                                                    src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                                    data-src="<?php echo $path, 'dist/img/full-img.jpg' ?>"
                                                    alt="Молоко пастеризованное 7%"
                                                    class="modal-item__thumb"></span>
                                            <figcaption class="modal-item__ttl">Молоко пастеризованное 7%</figcaption>
                                        </figure>
                                        <div class="modal-item__desc">Молоко – не просто ценный продукт питания, молоко
                                            это
                                            настоящее «чудо дивное», подаренное самой природой. Чудесным эликсиром
                                            жизни,
                                            молодости, красоты считается молоко у всех народов. В Индии корова, дающая
                                            молоко является священной и находится под охраной государства. Магазины
                                            предлагают покупателю очень широкий выбор молока от различных
                                            производителей, в
                                            различной упаковке.
                                            У натурального молока срок хранения не более 5 дней, такое молоко было
                                            подвержено тепловой обработке при невысокой температуре и максимально
                                            сохранило
                                            все полезные свойства (калоризатор). Оно рекомендовано к ежедневному
                                            потреблению, хотя и цена его выше.
                                        </div>
                                        <div class="modal-item__row">
                                            <div class="modal-item__left">
                                                <p class="modal-item__price-sale">390.00</p>
                                                <p class="modal-item__price-row"><span
                                                            class="modal-item__price">379.00</span><span
                                                            class="modal-item__value">за 200 гр.</span></p>
                                            </div>
                                            <div class="modal-item__right">
                                                <div class="quantity quantity_modal">
                                                    <a href="#"
                                                       class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                                    <div class="quantity__value">200 гр.</div>
                                                    <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                                </div>
                                                <a href="#" class="cart-btn cart-btn_modal">
                                                    <span class="cart-btn__cnt cart-btn__cnt_modal"
                                                          data-add="Добавлено">В корзину</span>
                                                    <span class="cart-btn__progress cart-btn__progress_modal"><span
                                                                class="cart-btn__inner cart-btn__inner_modal"></span></span>
                                                </a>
                                            </div>
                                        </div>
                                        <a href="#" class="items__mark items__mark_modal"><span
                                                    class="items__mark-value">Хит</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="items__itm" data-price="390.00" data-value="100" data-units="гр.">
                        <div class="items__small">
                            <figure class="items__image">
                                <a href="#" class="items__image-wrap"><img
                                            src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                            data-src="<?php echo $path, 'dist/img/itm-img.jpg' ?>"
                                            alt="Молоко пастеризованное 7%"
                                            class="items__thumb lazyload"></a>
                                <figcaption class="items__ttl"><a href="#" class="items__l">Молоко пастеризованное
                                        7%</a></figcaption>
                            </figure>
                            <div class="items__desc">Фермерское коровье молоко домашнее. Жирность 7,5%. Lorem ipsum
                                dolor sit amet, consectetur adipisicing elit. Cupiditate debitis eveniet inventore,
                                maiores nihil quisquam?
                            </div>
                            <div class="items__row">
                                <div class="items__left">
                                    <div class="items__price-sale">390.00</div>
                                    <div class="items__price">390.00</div>
                                    <div class="items__value">за 100 гр.</div>
                                </div>
                                <div class="items__right">
                                    <div class="quantity">
                                        <a href="#"
                                           class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                        <div class="quantity__value">100 гр.</div>
                                        <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                        <input type="text" class="bx_col_input quantity__count" name="quantity"
                                               value="1">
                                    </div>
                                    <a href="#" class="cart-btn">
                                        <span class="cart-btn__cnt" data-add="Добавлено">В корзину</span>
                                        <span class="cart-btn__progress"><span class="cart-btn__inner"></span></span>
                                    </a>
                                </div>
                            </div>
                            <a href="#" class="items__mark"><span
                                        class="items__mark-value">Хит</span></a>
                        </div>
                        <div class="modal-item">
                            <div class="modal-item__cnt">
                                <div class="modal-item__inner">
                                    <div class="modal-item__close"></div>
                                    <div class="modal-item__wrp">
                                        <figure class="modal-item__image">
                                        <span class="modal-item__image-wrap"><img
                                                    src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                                    data-src="<?php echo $path, 'dist/img/full-img.jpg' ?>"
                                                    alt="Молоко пастеризованное 7%"
                                                    class="modal-item__thumb"></span>
                                            <figcaption class="modal-item__ttl">Молоко пастеризованное 7%</figcaption>
                                        </figure>
                                        <div class="modal-item__desc">Молоко – не просто ценный продукт питания, молоко
                                            это
                                            настоящее «чудо дивное», подаренное самой природой. Чудесным эликсиром
                                            жизни,
                                            молодости, красоты считается молоко у всех народов. В Индии корова, дающая
                                            молоко является священной и находится под охраной государства. Магазины
                                            предлагают покупателю очень широкий выбор молока от различных
                                            производителей, в
                                            различной упаковке.
                                            У натурального молока срок хранения не более 5 дней, такое молоко было
                                            подвержено тепловой обработке при невысокой температуре и максимально
                                            сохранило
                                            все полезные свойства (калоризатор). Оно рекомендовано к ежедневному
                                            потреблению, хотя и цена его выше.
                                        </div>
                                        <div class="modal-item__row">
                                            <div class="modal-item__left">
                                                <p class="modal-item__price-sale">390.00</p>
                                                <p class="modal-item__price-row"><span
                                                            class="modal-item__price">379.00</span><span
                                                            class="modal-item__value">за 100 гр.</span></p>
                                            </div>
                                            <div class="modal-item__right">
                                                <div class="quantity quantity_modal">
                                                    <a href="#"
                                                       class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                                    <div class="quantity__value">100 гр.</div>
                                                    <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                                </div>
                                                <a href="#" class="cart-btn cart-btn_modal">
                                                    <span class="cart-btn__cnt cart-btn__cnt_modal"
                                                          data-add="Добавлено">В корзину</span>
                                                    <span class="cart-btn__progress cart-btn__progress_modal"><span
                                                                class="cart-btn__inner cart-btn__inner_modal"></span></span>
                                                </a>
                                            </div>
                                        </div>
                                        <a href="#" class="items__mark items__mark_modal"><span
                                                    class="items__mark-value">Хит</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="items__itm" data-price="390.00" data-value="1" data-units="литр">
                        <div class="items__small">
                            <figure class="items__image">
                                <a href="#" class="items__image-wrap"><img
                                            src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                            data-src="<?php echo $path, 'dist/img/itm-img.jpg' ?>"
                                            alt="Молоко пастеризованное 7%"
                                            class="items__thumb lazyload"></a>
                                <figcaption class="items__ttl"><a href="#" class="items__l">Молоко пастеризованное
                                        7%</a></figcaption>
                            </figure>
                            <div class="items__desc">Фермерское коровье молоко домашнее. Жирность 7,5%. Lorem ipsum
                                dolor sit amet, consectetur adipisicing elit. Cupiditate debitis eveniet inventore,
                                maiores nihil quisquam?
                            </div>
                            <div class="items__row">
                                <div class="items__left">
                                    <div class="items__price-sale">390.00</div>
                                    <div class="items__price">390.00</div>
                                    <div class="items__value">за 1 литр</div>
                                </div>
                                <div class="items__right">
                                    <div class="quantity">
                                        <a href="#"
                                           class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                        <div class="quantity__value">1 литр</div>
                                        <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                        <input type="text" class="bx_col_input quantity__count" name="quantity"
                                               value="1">
                                    </div>
                                    <a href="#" class="cart-btn">
                                        <span class="cart-btn__cnt" data-add="Добавлено">В корзину</span>
                                        <span class="cart-btn__progress"><span class="cart-btn__inner"></span></span>
                                    </a>
                                </div>
                            </div>
                            <a href="#" class="items__mark items__mark_sale"><span
                                        class="items__mark-value">%</span></a>
                        </div>
                        <div class="modal-item">
                            <div class="modal-item__cnt">
                                <div class="modal-item__inner">
                                    <div class="modal-item__close"></div>
                                    <div class="modal-item__wrp">
                                        <figure class="modal-item__image">
                                        <span class="modal-item__image-wrap"><img
                                                    src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                                    data-src="<?php echo $path, 'dist/img/full-img.jpg' ?>"
                                                    alt="Молоко пастеризованное 7%"
                                                    class="modal-item__thumb"></span>
                                            <figcaption class="modal-item__ttl">Молоко пастеризованное 7%</figcaption>
                                        </figure>
                                        <div class="modal-item__desc">Молоко – не просто ценный продукт питания, молоко
                                            это
                                            настоящее «чудо дивное», подаренное самой природой. Чудесным эликсиром
                                            жизни,
                                            молодости, красоты считается молоко у всех народов. В Индии корова, дающая
                                            молоко является священной и находится под охраной государства. Магазины
                                            предлагают покупателю очень широкий выбор молока от различных
                                            производителей, в
                                            различной упаковке.
                                            У натурального молока срок хранения не более 5 дней, такое молоко было
                                            подвержено тепловой обработке при невысокой температуре и максимально
                                            сохранило
                                            все полезные свойства (калоризатор). Оно рекомендовано к ежедневному
                                            потреблению, хотя и цена его выше.
                                        </div>
                                        <div class="modal-item__row">
                                            <div class="modal-item__left">
                                                <p class="modal-item__price-sale">390.00</p>
                                                <p class="modal-item__price-row"><span
                                                            class="modal-item__price">379.00</span><span
                                                            class="modal-item__value">за 1 литр</span></p>
                                            </div>
                                            <div class="modal-item__right">
                                                <div class="quantity quantity_modal">
                                                    <a href="#"
                                                       class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                                    <div class="quantity__value">1 литр</div>
                                                    <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                                </div>
                                                <a href="#" class="cart-btn cart-btn_modal">
                                                    <span class="cart-btn__cnt cart-btn__cnt_modal"
                                                          data-add="Добавлено">В корзину</span>
                                                    <span class="cart-btn__progress cart-btn__progress_modal"><span
                                                                class="cart-btn__inner cart-btn__inner_modal"></span></span>
                                                </a>
                                            </div>
                                        </div>
                                        <a href="#" class="items__mark items__mark_sale items__mark_modal"><span
                                                    class="items__mark-value">%</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="items__itm" data-price="390.00" data-value="200" data-units="гр.">
                        <div class="items__small">
                            <figure class="items__image">
                                <a href="#" class="items__image-wrap"><img
                                            src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                            data-src="<?php echo $path, 'dist/img/itm-img.jpg' ?>"
                                            alt="Молоко пастеризованное 7%"
                                            class="items__thumb lazyload"></a>
                                <figcaption class="items__ttl"><a href="#" class="items__l">Молоко пастеризованное
                                        7%</a></figcaption>
                            </figure>
                            <div class="items__desc">Фермерское коровье молоко домашнее. Жирность 7,5%. Lorem ipsum
                                dolor sit amet, consectetur adipisicing elit. Cupiditate debitis eveniet inventore,
                                maiores nihil quisquam?
                            </div>
                            <div class="items__row">
                                <div class="items__left">
                                    <div class="items__price-sale">390.00</div>
                                    <div class="items__price">390.00</div>
                                    <div class="items__value">за 200 гр.</div>
                                </div>
                                <div class="items__right">
                                    <div class="quantity">
                                        <a href="#"
                                           class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                        <div class="quantity__value">200 гр.</div>
                                        <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                        <input type="text" class="bx_col_input quantity__count" name="quantity"
                                               value="1">
                                    </div>
                                    <a href="#" class="cart-btn">
                                        <span class="cart-btn__cnt" data-add="Добавлено">В корзину</span>
                                        <span class="cart-btn__progress"><span class="cart-btn__inner"></span></span>
                                    </a>
                                </div>
                            </div>
                            <a href="#" class="items__mark"><span
                                        class="items__mark-value">Хит</span></a>
                        </div>
                        <div class="modal-item">
                            <div class="modal-item__cnt">
                                <div class="modal-item__inner">
                                    <div class="modal-item__close"></div>
                                    <div class="modal-item__wrp">
                                        <figure class="modal-item__image">
                                        <span class="modal-item__image-wrap"><img
                                                    src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                                    data-src="<?php echo $path, 'dist/img/full-img.jpg' ?>"
                                                    alt="Молоко пастеризованное 7%"
                                                    class="modal-item__thumb"></span>
                                            <figcaption class="modal-item__ttl">Молоко пастеризованное 7%</figcaption>
                                        </figure>
                                        <div class="modal-item__desc">Молоко – не просто ценный продукт питания, молоко
                                            это
                                            настоящее «чудо дивное», подаренное самой природой. Чудесным эликсиром
                                            жизни,
                                            молодости, красоты считается молоко у всех народов. В Индии корова, дающая
                                            молоко является священной и находится под охраной государства. Магазины
                                            предлагают покупателю очень широкий выбор молока от различных
                                            производителей, в
                                            различной упаковке.
                                            У натурального молока срок хранения не более 5 дней, такое молоко было
                                            подвержено тепловой обработке при невысокой температуре и максимально
                                            сохранило
                                            все полезные свойства (калоризатор). Оно рекомендовано к ежедневному
                                            потреблению, хотя и цена его выше.
                                        </div>
                                        <div class="modal-item__row">
                                            <div class="modal-item__left">
                                                <p class="modal-item__price-sale">390.00</p>
                                                <p class="modal-item__price-row"><span
                                                            class="modal-item__price">379.00</span><span
                                                            class="modal-item__value">за 200 гр.</span></p>
                                            </div>
                                            <div class="modal-item__right">
                                                <div class="quantity quantity_modal">
                                                    <a href="#"
                                                       class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                                    <div class="quantity__value">200 гр.</div>
                                                    <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                                </div>
                                                <a href="#" class="cart-btn cart-btn_modal">
                                                    <span class="cart-btn__cnt cart-btn__cnt_modal"
                                                          data-add="Добавлено">В корзину</span>
                                                    <span class="cart-btn__progress cart-btn__progress_modal"><span
                                                                class="cart-btn__inner cart-btn__inner_modal"></span></span>
                                                </a>
                                            </div>
                                        </div>
                                        <a href="#" class="items__mark items__mark_modal"><span
                                                    class="items__mark-value">Хит</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="items__itm" data-price="390.00" data-value="100" data-units="гр.">
                        <div class="items__small">
                            <figure class="items__image">
                                <a href="#" class="items__image-wrap"><img
                                            src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                            data-src="<?php echo $path, 'dist/img/itm-img.jpg' ?>"
                                            alt="Молоко пастеризованное 7%"
                                            class="items__thumb lazyload"></a>
                                <figcaption class="items__ttl"><a href="#" class="items__l">Молоко пастеризованное
                                        7%</a></figcaption>
                            </figure>
                            <div class="items__desc">Фермерское коровье молоко домашнее. Жирность 7,5%. Lorem ipsum
                                dolor sit amet, consectetur adipisicing elit. Cupiditate debitis eveniet inventore,
                                maiores nihil quisquam?
                            </div>
                            <div class="items__row">
                                <div class="items__left">
                                    <div class="items__price-sale">390.00</div>
                                    <div class="items__price">390.00</div>
                                    <div class="items__value">за 100 гр.</div>
                                </div>
                                <div class="items__right">
                                    <div class="quantity">
                                        <a href="#"
                                           class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                        <div class="quantity__value">100 гр.</div>
                                        <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                        <input type="text" class="bx_col_input quantity__count" name="quantity"
                                               value="1">
                                    </div>
                                    <a href="#" class="cart-btn">
                                        <span class="cart-btn__cnt" data-add="Добавлено">В корзину</span>
                                        <span class="cart-btn__progress"><span class="cart-btn__inner"></span></span>
                                    </a>
                                </div>
                            </div>
                            <a href="#" class="items__mark"><span
                                        class="items__mark-value">Хит</span></a>
                        </div>
                        <div class="modal-item">
                            <div class="modal-item__cnt">
                                <div class="modal-item__inner">
                                    <div class="modal-item__close"></div>
                                    <div class="modal-item__wrp">
                                        <figure class="modal-item__image">
                                        <span class="modal-item__image-wrap"><img
                                                    src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                                    data-src="<?php echo $path, 'dist/img/full-img.jpg' ?>"
                                                    alt="Молоко пастеризованное 7%"
                                                    class="modal-item__thumb"></span>
                                            <figcaption class="modal-item__ttl">Молоко пастеризованное 7%</figcaption>
                                        </figure>
                                        <div class="modal-item__desc">Молоко – не просто ценный продукт питания, молоко
                                            это
                                            настоящее «чудо дивное», подаренное самой природой. Чудесным эликсиром
                                            жизни,
                                            молодости, красоты считается молоко у всех народов. В Индии корова, дающая
                                            молоко является священной и находится под охраной государства. Магазины
                                            предлагают покупателю очень широкий выбор молока от различных
                                            производителей, в
                                            различной упаковке.
                                            У натурального молока срок хранения не более 5 дней, такое молоко было
                                            подвержено тепловой обработке при невысокой температуре и максимально
                                            сохранило
                                            все полезные свойства (калоризатор). Оно рекомендовано к ежедневному
                                            потреблению, хотя и цена его выше.
                                        </div>
                                        <div class="modal-item__row">
                                            <div class="modal-item__left">
                                                <p class="modal-item__price-sale">390.00</p>
                                                <p class="modal-item__price-row"><span
                                                            class="modal-item__price">379.00</span><span
                                                            class="modal-item__value">за 100 гр.</span></p>
                                            </div>
                                            <div class="modal-item__right">
                                                <div class="quantity quantity_modal">
                                                    <a href="#"
                                                       class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                                    <div class="quantity__value">100 гр.</div>
                                                    <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                                </div>
                                                <a href="#" class="cart-btn cart-btn_modal">
                                                    <span class="cart-btn__cnt cart-btn__cnt_modal"
                                                          data-add="Добавлено">В корзину</span>
                                                    <span class="cart-btn__progress cart-btn__progress_modal"><span
                                                                class="cart-btn__inner cart-btn__inner_modal"></span></span>
                                                </a>
                                            </div>
                                        </div>
                                        <a href="#" class="items__mark items__mark_modal"><span
                                                    class="items__mark-value">Хит</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="items__itm" data-price="390.00" data-value="1" data-units="литр">
                        <div class="items__small">
                            <figure class="items__image">
                                <a href="#" class="items__image-wrap"><img
                                            src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                            data-src="<?php echo $path, 'dist/img/itm-img.jpg' ?>"
                                            alt="Молоко пастеризованное 7%"
                                            class="items__thumb lazyload"></a>
                                <figcaption class="items__ttl"><a href="#" class="items__l">Молоко пастеризованное
                                        7%</a></figcaption>
                            </figure>
                            <div class="items__desc">Фермерское коровье молоко домашнее. Жирность 7,5%. Lorem ipsum
                                dolor sit amet, consectetur adipisicing elit. Cupiditate debitis eveniet inventore,
                                maiores nihil quisquam?
                            </div>
                            <div class="items__row">
                                <div class="items__left">
                                    <div class="items__price-sale">390.00</div>
                                    <div class="items__price">390.00</div>
                                    <div class="items__value">за 1 литр</div>
                                </div>
                                <div class="items__right">
                                    <div class="quantity">
                                        <a href="#"
                                           class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                        <div class="quantity__value">1 литр</div>
                                        <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                        <input type="text" class="bx_col_input quantity__count" name="quantity"
                                               value="1">
                                    </div>
                                    <a href="#" class="cart-btn">
                                        <span class="cart-btn__cnt" data-add="Добавлено">В корзину</span>
                                        <span class="cart-btn__progress"><span class="cart-btn__inner"></span></span>
                                    </a>
                                </div>
                            </div>
                            <a href="#" class="items__mark items__mark_sale"><span
                                        class="items__mark-value">%</span></a>
                        </div>
                        <div class="modal-item">
                            <div class="modal-item__cnt">
                                <div class="modal-item__inner">
                                    <div class="modal-item__close"></div>
                                    <div class="modal-item__wrp">
                                        <figure class="modal-item__image">
                                        <span class="modal-item__image-wrap"><img
                                                    src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                                    data-src="<?php echo $path, 'dist/img/full-img.jpg' ?>"
                                                    alt="Молоко пастеризованное 7%"
                                                    class="modal-item__thumb"></span>
                                            <figcaption class="modal-item__ttl">Молоко пастеризованное 7%</figcaption>
                                        </figure>
                                        <div class="modal-item__desc">Молоко – не просто ценный продукт питания, молоко
                                            это
                                            настоящее «чудо дивное», подаренное самой природой. Чудесным эликсиром
                                            жизни,
                                            молодости, красоты считается молоко у всех народов. В Индии корова, дающая
                                            молоко является священной и находится под охраной государства. Магазины
                                            предлагают покупателю очень широкий выбор молока от различных
                                            производителей, в
                                            различной упаковке.
                                            У натурального молока срок хранения не более 5 дней, такое молоко было
                                            подвержено тепловой обработке при невысокой температуре и максимально
                                            сохранило
                                            все полезные свойства (калоризатор). Оно рекомендовано к ежедневному
                                            потреблению, хотя и цена его выше.
                                        </div>
                                        <div class="modal-item__row">
                                            <div class="modal-item__left">
                                                <p class="modal-item__price-sale">390.00</p>
                                                <p class="modal-item__price-row"><span
                                                            class="modal-item__price">379.00</span><span
                                                            class="modal-item__value">за 1 литр</span></p>
                                            </div>
                                            <div class="modal-item__right">
                                                <div class="quantity quantity_modal">
                                                    <a href="#"
                                                       class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                                    <div class="quantity__value">1 литр</div>
                                                    <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                                </div>
                                                <a href="#" class="cart-btn cart-btn_modal">
                                                    <span class="cart-btn__cnt cart-btn__cnt_modal"
                                                          data-add="Добавлено">В корзину</span>
                                                    <span class="cart-btn__progress cart-btn__progress_modal"><span
                                                                class="cart-btn__inner cart-btn__inner_modal"></span></span>
                                                </a>
                                            </div>
                                        </div>
                                        <a href="#" class="items__mark items__mark_sale items__mark_modal"><span
                                                    class="items__mark-value">%</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="items__itm" data-price="390.00" data-value="200" data-units="гр.">
                        <div class="items__small">
                            <figure class="items__image">
                                <a href="#" class="items__image-wrap"><img
                                            src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                            data-src="<?php echo $path, 'dist/img/itm-img.jpg' ?>"
                                            alt="Молоко пастеризованное 7%"
                                            class="items__thumb lazyload"></a>
                                <figcaption class="items__ttl"><a href="#" class="items__l">Молоко пастеризованное
                                        7%</a></figcaption>
                            </figure>
                            <div class="items__desc">Фермерское коровье молоко домашнее. Жирность 7,5%. Lorem ipsum
                                dolor sit amet, consectetur adipisicing elit. Cupiditate debitis eveniet inventore,
                                maiores nihil quisquam?
                            </div>
                            <div class="items__row">
                                <div class="items__left">
                                    <div class="items__price-sale">390.00</div>
                                    <div class="items__price">390.00</div>
                                    <div class="items__value">за 200 гр.</div>
                                </div>
                                <div class="items__right">
                                    <div class="quantity">
                                        <a href="#"
                                           class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                        <div class="quantity__value">200 гр.</div>
                                        <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                        <input type="text" class="bx_col_input quantity__count" name="quantity"
                                               value="1">
                                    </div>
                                    <a href="#" class="cart-btn">
                                        <span class="cart-btn__cnt" data-add="Добавлено">В корзину</span>
                                        <span class="cart-btn__progress"><span class="cart-btn__inner"></span></span>
                                    </a>
                                </div>
                            </div>
                            <a href="#" class="items__mark"><span
                                        class="items__mark-value">Хит</span></a>
                        </div>
                        <div class="modal-item">
                            <div class="modal-item__cnt">
                                <div class="modal-item__inner">
                                    <div class="modal-item__close"></div>
                                    <div class="modal-item__wrp">
                                        <figure class="modal-item__image">
                                        <span class="modal-item__image-wrap"><img
                                                    src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                                    data-src="<?php echo $path, 'dist/img/full-img.jpg' ?>"
                                                    alt="Молоко пастеризованное 7%"
                                                    class="modal-item__thumb"></span>
                                            <figcaption class="modal-item__ttl">Молоко пастеризованное 7%</figcaption>
                                        </figure>
                                        <div class="modal-item__desc">Молоко – не просто ценный продукт питания, молоко
                                            это
                                            настоящее «чудо дивное», подаренное самой природой. Чудесным эликсиром
                                            жизни,
                                            молодости, красоты считается молоко у всех народов. В Индии корова, дающая
                                            молоко является священной и находится под охраной государства. Магазины
                                            предлагают покупателю очень широкий выбор молока от различных
                                            производителей, в
                                            различной упаковке.
                                            У натурального молока срок хранения не более 5 дней, такое молоко было
                                            подвержено тепловой обработке при невысокой температуре и максимально
                                            сохранило
                                            все полезные свойства (калоризатор). Оно рекомендовано к ежедневному
                                            потреблению, хотя и цена его выше.
                                        </div>
                                        <div class="modal-item__row">
                                            <div class="modal-item__left">
                                                <p class="modal-item__price-sale">390.00</p>
                                                <p class="modal-item__price-row"><span
                                                            class="modal-item__price">379.00</span><span
                                                            class="modal-item__value">за 200 гр.</span></p>
                                            </div>
                                            <div class="modal-item__right">
                                                <div class="quantity quantity_modal">
                                                    <a href="#"
                                                       class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                                    <div class="quantity__value">200 гр.</div>
                                                    <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                                </div>
                                                <a href="#" class="cart-btn cart-btn_modal">
                                                    <span class="cart-btn__cnt cart-btn__cnt_modal"
                                                          data-add="Добавлено">В корзину</span>
                                                    <span class="cart-btn__progress cart-btn__progress_modal"><span
                                                                class="cart-btn__inner cart-btn__inner_modal"></span></span>
                                                </a>
                                            </div>
                                        </div>
                                        <a href="#" class="items__mark items__mark_modal"><span
                                                    class="items__mark-value">Хит</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="items__itm" data-price="390.00" data-value="100" data-units="гр.">
                        <div class="items__small">
                            <figure class="items__image">
                                <a href="#" class="items__image-wrap"><img
                                            src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                            data-src="<?php echo $path, 'dist/img/itm-img.jpg' ?>"
                                            alt="Молоко пастеризованное 7%"
                                            class="items__thumb lazyload"></a>
                                <figcaption class="items__ttl"><a href="#" class="items__l">Молоко пастеризованное
                                        7%</a></figcaption>
                            </figure>
                            <div class="items__desc">Фермерское коровье молоко домашнее. Жирность 7,5%. Lorem ipsum
                                dolor sit amet, consectetur adipisicing elit. Cupiditate debitis eveniet inventore,
                                maiores nihil quisquam?
                            </div>
                            <div class="items__row">
                                <div class="items__left">
                                    <div class="items__price-sale">390.00</div>
                                    <div class="items__price">390.00</div>
                                    <div class="items__value">за 100 гр.</div>
                                </div>
                                <div class="items__right">
                                    <div class="quantity">
                                        <a href="#"
                                           class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                        <div class="quantity__value">100 гр.</div>
                                        <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                        <input type="text" class="bx_col_input quantity__count" name="quantity"
                                               value="1">
                                    </div>
                                    <a href="#" class="cart-btn">
                                        <span class="cart-btn__cnt" data-add="Добавлено">В корзину</span>
                                        <span class="cart-btn__progress"><span class="cart-btn__inner"></span></span>
                                    </a>
                                </div>
                            </div>
                            <a href="#" class="items__mark"><span
                                        class="items__mark-value">Хит</span></a>
                        </div>
                        <div class="modal-item">
                            <div class="modal-item__cnt">
                                <div class="modal-item__inner">
                                    <div class="modal-item__close"></div>
                                    <div class="modal-item__wrp">
                                        <figure class="modal-item__image">
                                        <span class="modal-item__image-wrap"><img
                                                    src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                                    data-src="<?php echo $path, 'dist/img/full-img.jpg' ?>"
                                                    alt="Молоко пастеризованное 7%"
                                                    class="modal-item__thumb"></span>
                                            <figcaption class="modal-item__ttl">Молоко пастеризованное 7%</figcaption>
                                        </figure>
                                        <div class="modal-item__desc">Молоко – не просто ценный продукт питания, молоко
                                            это
                                            настоящее «чудо дивное», подаренное самой природой. Чудесным эликсиром
                                            жизни,
                                            молодости, красоты считается молоко у всех народов. В Индии корова, дающая
                                            молоко является священной и находится под охраной государства. Магазины
                                            предлагают покупателю очень широкий выбор молока от различных
                                            производителей, в
                                            различной упаковке.
                                            У натурального молока срок хранения не более 5 дней, такое молоко было
                                            подвержено тепловой обработке при невысокой температуре и максимально
                                            сохранило
                                            все полезные свойства (калоризатор). Оно рекомендовано к ежедневному
                                            потреблению, хотя и цена его выше.
                                        </div>
                                        <div class="modal-item__row">
                                            <div class="modal-item__left">
                                                <p class="modal-item__price-sale">390.00</p>
                                                <p class="modal-item__price-row"><span
                                                            class="modal-item__price">379.00</span><span
                                                            class="modal-item__value">за 100 гр.</span></p>
                                            </div>
                                            <div class="modal-item__right">
                                                <div class="quantity quantity_modal">
                                                    <a href="#"
                                                       class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                                    <div class="quantity__value">100 гр.</div>
                                                    <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                                </div>
                                                <a href="#" class="cart-btn cart-btn_modal">
                                                    <span class="cart-btn__cnt cart-btn__cnt_modal"
                                                          data-add="Добавлено">В корзину</span>
                                                    <span class="cart-btn__progress cart-btn__progress_modal"><span
                                                                class="cart-btn__inner cart-btn__inner_modal"></span></span>
                                                </a>
                                            </div>
                                        </div>
                                        <a href="#" class="items__mark items__mark_modal"><span
                                                    class="items__mark-value">Хит</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="items__itm" data-price="390.00" data-value="1" data-units="литр">
                        <div class="items__small">
                            <figure class="items__image">
                                <a href="#" class="items__image-wrap"><img
                                            src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                            data-src="<?php echo $path, 'dist/img/itm-img.jpg' ?>"
                                            alt="Молоко пастеризованное 7%"
                                            class="items__thumb lazyload"></a>
                                <figcaption class="items__ttl"><a href="#" class="items__l">Молоко пастеризованное
                                        7%</a></figcaption>
                            </figure>
                            <div class="items__desc">Фермерское коровье молоко домашнее. Жирность 7,5%. Lorem ipsum
                                dolor sit amet, consectetur adipisicing elit. Cupiditate debitis eveniet inventore,
                                maiores nihil quisquam?
                            </div>
                            <div class="items__row">
                                <div class="items__left">
                                    <div class="items__price-sale">390.00</div>
                                    <div class="items__price">390.00</div>
                                    <div class="items__value">за 1 литр</div>
                                </div>
                                <div class="items__right">
                                    <div class="quantity">
                                        <a href="#"
                                           class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                        <div class="quantity__value">1 литр</div>
                                        <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                        <input type="text" class="bx_col_input quantity__count" name="quantity"
                                               value="1">
                                    </div>
                                    <a href="#" class="cart-btn">
                                        <span class="cart-btn__cnt" data-add="Добавлено">В корзину</span>
                                        <span class="cart-btn__progress"><span class="cart-btn__inner"></span></span>
                                    </a>
                                </div>
                            </div>
                            <a href="#" class="items__mark items__mark_sale"><span
                                        class="items__mark-value">%</span></a>
                        </div>
                        <div class="modal-item">
                            <div class="modal-item__cnt">
                                <div class="modal-item__inner">
                                    <div class="modal-item__close"></div>
                                    <div class="modal-item__wrp">
                                        <figure class="modal-item__image">
                                        <span class="modal-item__image-wrap"><img
                                                    src="<?php echo $path, 'dist/img/loading.jpg' ?>"
                                                    data-src="<?php echo $path, 'dist/img/full-img.jpg' ?>"
                                                    alt="Молоко пастеризованное 7%"
                                                    class="modal-item__thumb"></span>
                                            <figcaption class="modal-item__ttl">Молоко пастеризованное 7%</figcaption>
                                        </figure>
                                        <div class="modal-item__desc">Молоко – не просто ценный продукт питания, молоко
                                            это
                                            настоящее «чудо дивное», подаренное самой природой. Чудесным эликсиром
                                            жизни,
                                            молодости, красоты считается молоко у всех народов. В Индии корова, дающая
                                            молоко является священной и находится под охраной государства. Магазины
                                            предлагают покупателю очень широкий выбор молока от различных
                                            производителей, в
                                            различной упаковке.
                                            У натурального молока срок хранения не более 5 дней, такое молоко было
                                            подвержено тепловой обработке при невысокой температуре и максимально
                                            сохранило
                                            все полезные свойства (калоризатор). Оно рекомендовано к ежедневному
                                            потреблению, хотя и цена его выше.
                                        </div>
                                        <div class="modal-item__row">
                                            <div class="modal-item__left">
                                                <p class="modal-item__price-sale">390.00</p>
                                                <p class="modal-item__price-row"><span
                                                            class="modal-item__price">379.00</span><span
                                                            class="modal-item__value">за 1 литр</span></p>
                                            </div>
                                            <div class="modal-item__right">
                                                <div class="quantity quantity_modal">
                                                    <a href="#"
                                                       class="quantity__btn quantity__btn_minus quantity__btn_disable">-</a>
                                                    <div class="quantity__value">1 литр</div>
                                                    <a href="#" class="quantity__btn quantity__btn_plus">+</a>
                                                </div>
                                                <a href="#" class="cart-btn cart-btn_modal">
                                                    <span class="cart-btn__cnt cart-btn__cnt_modal"
                                                          data-add="Добавлено">В корзину</span>
                                                    <span class="cart-btn__progress cart-btn__progress_modal"><span
                                                                class="cart-btn__inner cart-btn__inner_modal"></span></span>
                                                </a>
                                            </div>
                                        </div>
                                        <a href="#" class="items__mark items__mark_sale items__mark_modal"><span
                                                    class="items__mark-value">%</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="items__loader">
                    <div class="cs-loader in">
                        <div class="cs-loader__inner">
                            <label class="cs-loader__item">●</label>
                            <label class="cs-loader__item">●</label>
                            <label class="cs-loader__item">●</label>
                            <label class="cs-loader__item">●</label>
                            <label class="cs-loader__item">●</label>
                            <label class="cs-loader__item">●</label>
                        </div>
                    </div>
                    <div class="loader">
                        <div class="loader__dot"></div>
                        <div class="loader__dot"></div>
                        <div class="loader__dot"></div>
                        <div class="loader__dot"></div>
                        <div class="loader__dot"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require_once 'footer.php' ?>