var gulp = require('gulp'),
    sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),
    rename = require("gulp-rename"),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    babel = require('gulp-babel'),
    webpack = require('webpack-stream'),
    mainBowerFiles = require('main-bower-files'),
    gutil = require('gulp-util'),
    svgSprite = require("gulp-svg-sprites"),
    sftp = require('gulp-sftp'),
    /*ftp = require('gulp-ftp'),*/
    path = '',
    connectJS = {
        host: '',
        port: '',
        user: '',
        pass: '',
        remotePath: ''
    },
    connectCSS = {
        host: '',
        port: '',
        user: '',
        pass: '',
        remotePath: ''
    },
    connectEditor = {
        host: '',
        port: '',
        user: '',
        pass: '',
        remotePath: ''
    };

gulp.task('watch', ['bowerJS', 'bowerCSS'], function () {
    gulp.watch(path + 'assets/src/scss/library/*.scss', ['scss']);
    gulp.watch(path + 'assets/src/scss/head/*.scss', ['hscss']);
    gulp.watch(path + 'assets/src/scss/head.scss', ['hscss']);
    gulp.watch(path + 'assets/src/scss/main/*.scss', ['mscss']);
    gulp.watch(path + 'assets/src/scss/main.scss', ['mscss']);
    gulp.watch(path + 'assets/src/scss/fonts.scss', ['fscss']);
    gulp.watch(path + 'assets/src/scss/editor-style.scss', ['editor']);
    gulp.watch(path + 'assets/src/js/*.js', ['js']);
    /*gulp.watch(path + 'assets/src/js/init.js', ['webpack']);*/
    gulp.watch(path + 'assets/src/js/fonts.js', ['fjs']);

});

gulp.task('scss', function () {
    return gulp.src(path + 'assets/src/scss/*.scss')
        .pipe(sass(/*{outputStyle: 'compressed'}*/).on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(rename({extname: ".min.css"}))
        .pipe(gulp.dest(path + 'assets/dist/css'))
        .pipe(sftp(connectCSS))
        .pipe(gutil.noop());
});

gulp.task('hscss', function () {
    return gulp.src(path + 'assets/src/scss/head.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(rename({extname: ".min.css"}))
        .pipe(gulp.dest(path + 'assets/dist/css'))
        .pipe(sftp(connectCSS))
        .pipe(gutil.noop());
});

gulp.task('mscss', function () {
    return gulp.src(path + 'assets/src/scss/main.scss')
        .pipe(sass(/*{outputStyle: 'compressed'}*/).on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(rename({extname: ".min.css"}))
        .pipe(gulp.dest(path + 'assets/dist/css'))
        .pipe(sftp(connectCSS))
        .pipe(gutil.noop());
});

gulp.task('fscss', function () {
    return gulp.src(path + 'assets/src/scss/fonts.scss')
        .pipe(sass(/*{outputStyle: 'compressed'}*/).on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(rename({extname: ".min.css"}))
        .pipe(gulp.dest(path + 'assets/dist/css'))
        .pipe(sftp(connectCSS))
        .pipe(gutil.noop());
});

gulp.task('js', function () {
    gulp.src([

        /*        path + 'assets/src/js/modernizr.custom.js',
         path + 'assets/src/js/masonry.pkgd.min.js',
         path + 'assets/src/js/imagesloaded.js',
         path + 'assets/src/js/classie.js',
         path + 'assets/src/js/AnimOnScroll.js',*/
        path + 'assets/src/js/imagesloaded.pkgd.min.js',
        path + 'assets/src/js/anime.min.js',
        path + 'assets/src/js/main.js',

        path + 'assets/src/js/jquery.js',
        path + 'assets/src/js/maskedinput.js',
        path + 'assets/src/js/swiper.js',

        /*        path + 'assets/src/js/vanilla.datepicker.js',*/
        path + 'assets/src/js/axios.js',
        path + 'assets/src/js/lightgallery.min.js',
        path + 'assets/src/js/lg-thumbnail.js',
        /*  path + 'assets/src/js/lg-video.js',*/
        path + 'assets/src/js/init.js',
    ])
        .pipe(concat('all.js'))
        .pipe(uglify())
        /*        .pipe(babel({
         presets: ['es2015']
         }))*/
        .pipe(rename("app.min.js"))
        .pipe(gulp.dest(path + 'assets/dist/js/'))
        .pipe(sftp(connectJS))
        .pipe(gutil.noop());
});

gulp.task('fjs', function () {
    gulp.src([
        path + 'assets/src/js/fonts.js',
    ])
        .pipe(uglify())
        .pipe(rename({extname: ".min.js"}))
        .pipe(gulp.dest(path + 'assets/dist/js/'))
        .pipe(sftp(connectJS))
        .pipe(gutil.noop());
});

gulp.task('webpack', function () {

    gulp.src([
        path + 'assets/src/js/swiper.js',
        /*path + 'assets/src/js/axios.js',*/
        path + 'assets/src/js/init.js'
    ])
        .pipe(webpack(require('./webpack.config.js')))
        .pipe(concat('all.js'))
        .pipe(uglify())
        .pipe(rename("app.min.js"))
        .pipe(gulp.dest(path + 'assets/dist/js/'))
        .pipe(sftp(connectJS))
        .pipe(gutil.noop());
});

gulp.task('editor', function () {
    return gulp.src(path + 'assets/src/scss/editor-style.scss')
        .pipe(sass(/*{outputStyle: 'compressed'}*/).on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(rename({extname: ".css"}))
        .pipe(gulp.dest(path))
        .pipe(sftp(connectEditor))
        .pipe(gutil.noop());
});

gulp.task('bowerJS', function () {
    return gulp.src(mainBowerFiles('**/*.js'))
        .pipe(gulp.dest(path + 'assets/src/js/'));
});

gulp.task('bowerCSS', function () {
    return gulp.src(mainBowerFiles('**/*.css'))
        .pipe(gulp.dest(path + 'assets/src/css/'));
});

gulp.task('sprites', function () {
    return gulp.src(path + 'assets/src/svg/*.svg')
        .pipe(svgSprite({
            baseSize: 10,
            padding: 10,
            spacing: {
                padding: 10
            },
        }))
        .pipe(gulp.dest(path + "assets/dist"));
});