<!doctype html>
<html lang="ru-RU">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Воронцовская кулинария</title>
    <meta name="theme-color" content="#8eefd7">
    <link rel="icon" href="/local/templates/cookery/assets/dist/img/cropped-favicon-32x32.png" sizes="32x32"/>
    <link rel="icon" href="/local/templates/cookery/assets/dist/img/cropped-favicon-192x192.png" sizes="192x192"/>
    <link rel="apple-touch-icon-precomposed"
          href="/local/templates/cookery/assets/dist/img/cropped-favicon-180x180.png"/>
    <meta name="msapplication-TileImage"
          content="/local/templates/cookery/assets/dist/img/cropped-favicon-270x270.png"/>
    <meta name="robots" content="noindex,nofollow"/>
    <script>
        (function () {
            if (sessionStorage.foutFontsLoaded) {
                document.documentElement.className += " fonts-loaded";
                return;
            }
            <?php require_once dirname(__FILE__) . '/local/templates/cookery/assets/src/js/fontfaceobserver.js'; ?>
            <?php require_once dirname(__FILE__) . '/local/templates/cookery/assets/dist/js/fonts.min.js'; ?>
        })();
    </script>
    <style><?php require_once dirname(__FILE__) . '/local/templates/cookery/assets/dist/css/head.min.css'; ?></style>
    <?php $path = '/local/templates/cookery/assets/'; ?>
    <script defer src='https://www.google.com/recaptcha/api.js'></script>
    <script defer src="/local/templates/cookery/assets/dist/js/app.min.js"></script>
</head>
<body>
<div class="wrp">
    <main class="main">
        <header class="hdr">
            <div class="cnt">
                <div class="hdr__cnt">
                    <div class="hdr__contact">
                        <a href="/" class="logo"><img src="/local/templates/cookery/assets/dist/img/main-logo.svg"
                                                      alt="logo" class="logo__img"></a>
                        <a href="tel:+74957615577" class="hdr__phone">+7 (495) 761-55-77</a>
                        <address class="hdr__address">Москва, Воронцовская 21</address>
                    </div>
                    <div class="hdr__name">
                        <a href="/" class="logo-name"><img src="/local/templates/cookery/assets/dist/img/logo-text.svg"
                                                           alt="logo" class="logo-name__img"></a>
                        <span class="logo-name__txt">Здоровое питание</span>
                    </div>
                    <div class="hdr__shop-wrp">
                        <div class="hdr__shop">
                            <div class="cnt cnt_head">
                                <div class="nav-row">
                                    <div class="burger">
                                        <div class="burger__btn"><span></span></div>
                                    </div>
                                    <div class="i-bskt">
                                        <div class="i-bskt__count">12</div>
                                    </div>
                                    <div class="i-user"></div>
                                </div>
                            </div>
                            <div class="hdr__content">
                                <div class="cnt cnt_head">
                                    <nav class="m-menu">
                                        <ul class="m-menu__lst">
                                            <li class="m-menu__itm"><a href="#" class="m-menu__l">Меню от шефа</a></li>
                                            <li class="m-menu__itm"><a href="#" class="m-menu__l">Кулинария</a></li>
                                            <li class="m-menu__itm"><a href="#" class="m-menu__l m-menu__l_in">Наша
                                                    ферма</a></li>
                                            <li class="m-menu__itm"><a href="#" class="m-menu__l">Пекарня/десерты</a>
                                            </li>
                                            <li class="m-menu__itm"><a href="#" class="m-menu__l">Овощи/Фрукты</a></li>
                                            <li class="m-menu__itm"><a href="#" class="m-menu__l">Лучшее детям</a></li>
                                            <li class="m-menu__itm"><a href="#" class="m-menu__l">Лидеры продаж</a></li>
                                            <li class="m-menu__itm"><a href="#" class="m-menu__l">Предложение дня</a>
                                            </li>
                                            <li class="m-menu__itm"><a href="#" class="m-menu__l">Новинки</a></li>
                                            <li class="m-menu__itm"><a href="#" class="m-menu__l">О нас</a></li>
                                        </ul>
                                    </nav>
                                    <div class="user-cart">
                                        <div class="m-user">
                                            <?php if (true): ?>
                                                <!-- Неавторизирован -->
                                                <a href="#" class="m-user__enter auth-btn"
                                                   data-target="auth-login">Вход</a>
                                                <a href="#" class="m-user__reg auth-btn" data-target="auth-reg">Регистрация</a>
                                            <?php endif; ?>
                                            <?php if (false): ?>
                                                <!-- Авторизирован -->
                                                <p class="m-user__name">Алексей<a href="#" class="m-user__out"></a></p>
                                                <p class="m-user__phone auth-btn" data-target="tgl-phone">+7 901 902 33
                                                    44</p>
                                            <?php endif; ?>
                                        </div>
                                        <?php if ($page == 'home'): ?>
                                            <div class="m-basket">
                                                <p class="m-basket__ttl">Моя корзина<span class="m-basket__icon"><span
                                                                class="m-basket__count">12</span></span></p>
                                                <?php if (false): ?>
                                                    <button class="m-basket__btn">Оформить</button>
                                                <?php endif; ?>
                                                <?php if (true) : ?>
                                                    <!--Если не авторизован-->
                                                    <button disabled class="m-basket__btn">Оформить</button>
                                                <?php endif; ?>

                                                <div class="m-basket__txt">Ваша копилка</div>
                                                <?php if (false): ?>
                                                    <p class="m-basket__price">5780.00</p>
                                                <?php endif; ?>
                                                <?php if (true): ?>
                                                    <!--Если не авторизован-->
                                                    <p class="m-basket__about"><a href="#"
                                                                                  class="m-basket__about-l  auth-btn"
                                                                                  data-target="money-box">Что
                                                            это?</a></p>
                                                <?php endif; ?>
                                                <p class="m-basket__row">
                                                    <a href="#" class="m-basket__l">Оплата</a><a href="#"
                                                                                                 class="m-basket__l m-basket__l_r">Доставка</a>
                                                </p>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="auth-modal">
            <div class="auth-modal__cnt"><!-- auth-modal__cnt_tgl-phone--><!--auth-modal__cnt_auth-reg-->
                <div class="auth-modal__inner">
                    <div class="auth-modal__close"></div>
                    <div class="auth-modal__wrp">
                        <div class="auth-modal__el money-box">
                            <div class="money-box__container">
                                <p class="money-box__ttl">Ваша копилка</p>
                                <p class="money-box__txt">После каждой покупки на Ваш аккаунт накапливаются бонусы,
                                    которые Вы можете потратить в любой момент без ограничений.</p>
                                <p class="money-box__sttl">Наша система бонусов</p>
                                <p class="money-box__txt">При покупке до 500¤ - накопление 3%;<br>
                                    от 500¤ до 2000¤ - 5%;<br>
                                    от 2000¤ до 5000¤ - 7%;<br>
                                    от 5000¤ и выше - 10%.</p>
                                <p class="money-box__txt">
                                    Для начисления бонусов в Вашу копилку Вам необходимо авторизироваться.
                                </p>
                            </div>
                            <div class="money-box__row">
                                <p class="money-box__auth auth-btn" data-target="auth-login">Авторизация</p>
                                <p class="money-box__back auth-modal__close-support">Вернуться к покупкам</p>
                            </div>
                        </div>
                        <div class="auth-modal__el tgl-phone"><!--tgl-phone_answer-->
                            <p class="tgl-phone__ttl">Сменить номер<br> телефона</p>
                            <div class="tgl-phone__form">
                                <form action="" method="post" class="auth-form">
                                    <fieldset class="auth-form__row  auth-form__row_pass">
                                        <label class="auth-form__eye"></label>
                                        <input type="password" name="authForm[pass]" class="auth-form__input"
                                               placeholder="Пароль">
                                    </fieldset>
                                    <fieldset class="auth-form__row">
                                        <input type="text" name="authForm[newphone]" class="auth-form__input phone"
                                               placeholder="Новый номер телефона">
                                    </fieldset>
                                    <fieldset class="auth-form__row auth-form__row_tgl-phone">
                                        <button class="auth-form__submit auth-form__submit_tgl-phone">Изменить</button>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="tgl-phone__answer">
                                <p class="tgl-phone__txt">Номер телефона изменен! <br>Вы можете продолжить свои покупки
                                    <br> или перейти в корзину.</p>
                                <a href="#" class="tgl-phone__to-cart">В корзину</a>
                            </div>
                        </div>
                        <div class="auth-modal__el auth-reg"><!--auth-reg_answer-->
                            <p class="auth-reg__ttl">Регистрация</p>
                            <div class="auth-reg__form">
                                <form action="" method="post" class="auth-form">
                                    <fieldset class="auth-form__row"><input type="text" name="authForm[name]"
                                                                            class="auth-form__input" placeholder="Имя">
                                    </fieldset>
                                    <fieldset class="auth-form__row"><input type="text" name="authForm[phone]"
                                                                            class="auth-form__input phone"
                                                                            placeholder="Номер телефона"></fieldset>
                                    <fieldset class="auth-form__row auth-form__row_pass">
                                        <label class="auth-form__eye"></label>
                                        <input type="password" name="authForm[pass]"
                                               class="auth-form__input"
                                               placeholder="Пароль">
                                    </fieldset>
                                    <fieldset class="auth-form__row auth-form__row_pass">
                                        <label class="auth-form__eye"></label>
                                        <input type="password" name="authForm[rep_pass]"
                                               class="auth-form__input"
                                               placeholder="Повторить пароль"></fieldset>
                                    <fieldset class="auth-form__row auth-form__row_captcha">
                                        <div class="auth-reg__captcha g-recaptcha"
                                             data-sitekey="6LcOsB0UAAAAAHIKl0z2P3D85J3oN-DlTQJbp_7T"></div>
                                    </fieldset>
                                    <fieldset class="auth-form__row auth-form__row_auth-reg">
                                        <button class="auth-form__submit auth-form__submit_auth-reg">
                                            Зарегистрироваться
                                        </button>
                                        <p class="auth-reg__login auth-btn" data-target="auth-login">Авторизация</p>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="auth-reg__answer">
                                <p class="auth-reg__txt">Регистрация завершена!<br> Вы можете продолжить свои
                                    покупки<br> или авторизироваться.</p>
                                <a href="#" class="auth-reg__to-cart">Войти</a>
                            </div>
                        </div>
                        <div class="auth-modal__el auth-pass">
                            <p class="auth-pass__ttl">Вход</p>
                            <div class="auth-pass__form auth-pass__form_first">
                                <form action="?" method="post" class="auth-form">
                                    <fieldset class="auth-form__row">
                                        <input type="text" name="authForm[sms_phone]" class="auth-form__input phone"
                                               placeholder="Телефон"></fieldset>
                                    <fieldset class="auth-form__row auth-form__row_last">
                                        <button class="auth-form__submit auth-form__submit_tgl">Изменить пароль</button>
                                        <p class="auth-pass__link auth-btn" data-target="auth-login">Назад</p>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="auth-pass__form auth-pass__form_sms">
                                <form action="?" method="post" class="auth-form">
                                    <p class="auth-pass__txt">В течении 5 минут на Ваш номер придет смс с новым
                                        паролем.</p>
                                    <fieldset class="auth-form__row">
                                        <input type="password" name="authForm[sms_password]" class="auth-form__input "
                                               placeholder="Пароль"></fieldset>
                                    <fieldset class="auth-form__row auth-form__row_last">
                                        <button class="auth-form__submit auth-form__submit_tgl">Войти</button>
                                        <p class="auth-pass__link auth-btn" data-pass="auth-pass__form_not-sms"
                                           data-target="auth-pass">Не пришла смс?</p>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="auth-pass__form auth-pass__form_not-sms">
                                <form action="?" method="post" class="auth-form">
                                    <p class="auth-pass__txt">Проверьте, верно ли набран номер мобильного телефона и
                                        повторите попытку. Если проблема останется нерешенной, свяжитесь с нами</p>
                                    <fieldset class="auth-form__row">
                                        <input type="text" name="authForm[sms_phone]" class="auth-form__input phone"
                                               placeholder="Телефон"></fieldset>
                                    <fieldset class="auth-form__row auth-form__row_last">
                                        <button class="auth-form__submit auth-form__submit_tgl auth-btn"
                                                data-pass="auth-pass__form_sms" data-target="auth-pass">Получить смс
                                        </button>
                                        <p class="auth-pass__link">Перейти в Контакты</p>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        <div class="auth-modal__el auth-login">
                            <p class="auth-login__ttl">Вход</p>
                            <div class="auth-login__form">
                                <form action="" method="post" class="auth-form">
                                    <fieldset class="auth-form__row"><input type="text" name="authForm[phone]"
                                                                            class="auth-form__input phone"
                                                                            placeholder="Номер телефона"></fieldset>
                                    <fieldset class="auth-form__row auth-form__row_pass"><label
                                                class="auth-form__eye"></label><input type="password"
                                                                                      name="authForm[pass]"
                                                                                      class="auth-form__input"
                                                                                      placeholder="Пароль"></fieldset>
                                    <fieldset class="auth-form__row auth-form__row_hide auth-form__row_login-captcha">
                                        <!--<div class="auth-login__captcha g-recaptcha"
                                             data-sitekey="6LcOsB0UAAAAAHIKl0z2P3D85J3oN-DlTQJbp_7T"></div>-->
                                    </fieldset>
                                    <fieldset class="auth-form__row auth-form__row_login">
                                        <button class="auth-form__submit auth-form__submit_login">Войти</button>
                                        <p class="auth-login__lost auth-btn" data-pass="auth-pass__form_first"
                                           data-target="auth-pass">Забыли пароль?</p>
                                    </fieldset>
                                    <fieldset class="auth-form__row auth-form__row_last">
                                        <p class="auth-login__reg auth-btn" data-target="auth-reg">
                                            Зарегистрироваться</p>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>