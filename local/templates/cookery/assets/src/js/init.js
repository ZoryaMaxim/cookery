/*(function ($) {
 $(document).ready(function () {

 });
 })(jQuery);*/

/*$(document).ready(function () {
 //addPhoneMask($);
 });*/


document.addEventListener("DOMContentLoaded", function (event) {
    localStorage.clear();
    loadSVG();
    loadMainCss();
    clearPlaceholder();
    burger();
    itmHelpers();
    leftMenu();
    resizeInit();
    authModal();
    inputMasks();
    passwordHelper();
    loadMap();
    resizeInit();
    /*    newsAnimation();
     sendFormOrder();
     scrollHeader();

     gallery();
     tableWrap();
     loadMap();
     modalOrder();
     modalClose();*/
});

window.onload = function () {
    setTimeout(function () {
        fixedMenuDesktop();
        fixedMenuMobile();
    }, 500);
};

function passwordHelper() {
    var eye = document.querySelectorAll('.auth-form__row_pass');
    [].slice.call(eye).forEach(function (el) {
        el.querySelector('.auth-form__input').addEventListener('keyup', function () {
            if (this.value.length > 0) {
                this.previousElementSibling.classList.add('auth-form__eye_show')
            }
            else {
                this.previousElementSibling.classList.remove('auth-form__eye_show')
            }
        });
        el.querySelector('.auth-form__eye').addEventListener('click', function (e) {
            if (this.classList.contains('auth-form__eye_in')) {
                this.classList.remove('auth-form__eye_in');
                this.nextSibling.type = 'password';
            } else {
                this.classList.add('auth-form__eye_in');
                this.nextSibling.type = 'text';
            }
        })
    });
}

function inputMasks() {
    var phones = document.querySelectorAll('input.phone'),
        phoneMask = ['+', '7', ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];
    [].slice.call(phones).forEach(function (phone) {
        phone.addEventListener('focus', function (e) {
            phone.placeholder = '+7 (___) ___-__-__';
        });
        vanillaTextMask.maskInput({
            inputElement: phone,
            keepCharPositions: true,
            mask: phoneMask
        })
    })
}

function popupHeight() {
    var h = window.innerHeight,
        products = document.querySelectorAll('.modal-item__wrp'),
        auth = document.querySelector('.auth-modal__wrp');
    if (products) {
        [].slice.call(products).forEach(function (el) {
            el.style.maxHeight = h - 100 + 'px';
        });
        auth.style.maxHeight = h - 100 + 'px';
    }

}

function authModal() {
    var btn = document.querySelectorAll('.auth-btn'),
        modal = document.querySelector('.auth-modal'),
        cart = document.querySelector('.m-basket'),
        pass = modal.querySelectorAll('.auth-pass__form'),
        elments = modal.querySelectorAll('.auth-modal__el'),
        close = modal.querySelectorAll('.auth-modal__close, .auth-modal__close-support'),
        modalCnt = modal.querySelector('.auth-modal__cnt');
    if (btn) {
        [].slice.call(btn).forEach(function (el, pos) {

            el.addEventListener('click', function (e) {
                e.preventDefault();
                [].slice.call(elments).forEach(function (bl) {
                    bl.classList.remove('auth-target');
                    if (bl.classList.contains(el.dataset.target)) {
                        bl.classList.add('auth-target');
                        if (el.dataset.target == 'auth-pass') {
                            [].slice.call(pass).forEach(function (passForm) {
                                passForm.classList.remove('auth-target');
                                if (passForm.classList.contains(el.dataset.pass)) passForm.classList.add('auth-target');
                            });
                        }
                    }
                });
                modalCnt.style.top = cart.getBoundingClientRect().top + 'px';
                modalCnt.style.left = cart.getBoundingClientRect().left + 'px';
                modal.classList.add('auth-modal_move');
                setTimeout(function () {
                    modal.classList.add('auth-modal_in');
                }, 100);
            });

            modalCnt.addEventListener('click', function (e) {
                e.stopPropagation();
            });
            /*
             Закрываем окно
             */
            [].slice.call(close).forEach(function (cls) {
                cls.addEventListener('click', function (e) {
                    modalCnt.style.top = cart.getBoundingClientRect().top + 'px';
                    modalCnt.style.left = cart.getBoundingClientRect().left + 'px';
                    modal.classList.remove('auth-modal_in');
                    setTimeout(function () {
                        modal.classList.remove('auth-modal_move');
                    }, 400);
                });
            })

            modal.addEventListener('click', function (e) {
                modalCnt.style.top = cart.getBoundingClientRect().top + 'px';
                modalCnt.style.left = cart.getBoundingClientRect().left + 'px';
                modal.classList.remove('auth-modal_in');
                setTimeout(function () {
                    modal.classList.remove('auth-modal_move');
                }, 400);
            });
        })
    }
}

function resizeInit() {
    window.addEventListener("resize", function () {
        leftMenu();
        popupHeight();
    });
    window.addEventListener("orientationchange", function () {
        popupHeight();
    });
}

function itmHelpers() {
    modalItem();
    addToCart();
    popupHeight();
}

function modalItem() {
    var item = document.querySelectorAll('.items__itm:not(items__itm_e)');
    if (item) {
        [].slice.call(item).forEach(function (el, pos) {
            var modal = el.querySelector('.modal-item'),
                small = el.querySelector('.items__small'),
                close = el.querySelector('.modal-item__close'),
                img = el.querySelector('.modal-item__thumb'),
                mdlCart = el.querySelector('.cart-btn_modal'),
                cart = el.querySelector('.items__right .cart-btn'),
                l = el.querySelector('.items__image'),
                modalCnt = modal.querySelector('.modal-item__cnt');

            el.classList.add('items__itm_e');

            quantity(el);

            /*
             Иммитация клика кнопки корзины
             */
            mdlCart.addEventListener('click', function (e) {
                cart.click();
            });

            /*
             Открытия окна
             */
            l.addEventListener('click', function (e) {
                e.preventDefault();
                if (!img.classList.contains('modal-item__thumb_load')) {
                    img.src = img.dataset.src;
                    img.classList.add('modal-item__thumb_load');
                }
                modalCnt.style.top = el.offsetTop - window.scrollY + 'px';
                modalCnt.style.left = el.offsetLeft - window.scrollX + 'px';
                modal.classList.add('modal-item_move');
                setTimeout(function () {
                    modal.classList.add('modal-item_in');
                    small.classList.add('items__small_in');
                }, 100);
            });

            modalCnt.addEventListener('click', function (e) {
                e.stopPropagation();
            });

            /*
             Закрываем окно
             */
            close.addEventListener('click', function (e) {
                modalCnt.style.top = el.offsetTop - window.scrollY + 'px';
                modalCnt.style.left = el.offsetLeft - window.scrollX + 'px';
                small.classList.remove('items__small_in');
                modal.classList.remove('modal-item_in');
                setTimeout(function () {
                    modal.classList.remove('modal-item_move');
                }, 400);
            });

            modal.addEventListener('click', function (e) {
                modalCnt.style.top = el.offsetTop - window.scrollY + 'px';
                modalCnt.style.left = el.offsetLeft - window.scrollX + 'px';
                small.classList.remove('items__small_in');
                this.classList.remove('modal-item_in');

                setTimeout(function () {
                    modal.classList.remove('modal-item_move');
                }, 400);
            });
        });
    }
}

function addToCart() {
    var btn = document.querySelectorAll('.cart-btn:not(.cart-btn_e');
    if (btn) {
        [].slice.call(btn).forEach(function (el, pos) {
            el.classList.add('cart-btn_e');
            el.addEventListener('click', function (e) {
                e.preventDefault();
                el.classList.add('cart-btn_hover');
                el.querySelector('.cart-btn__inner').classList.add('cart-btn__inner_load');
                setTimeout(function () {
                    el.querySelector('.cart-btn__cnt').classList.add('cart-btn__cnt_load');
                    el.classList.remove('cart-btn_hover');
                }, 2000);
                setTimeout(function () {
                    el.querySelector('.cart-btn__inner').classList.remove('cart-btn__inner_load');
                    el.querySelector('.cart-btn__cnt').classList.remove('cart-btn__cnt_load');
                    el.querySelector('.cart-btn__inner').classList.add('cart-btn__inner_add');


                }, 4000);
                setTimeout(function () {

                    el.querySelector('.cart-btn__inner').classList.remove('cart-btn__inner_add');

                }, 4300);
            });
        });
    }
}

function quantity(el) {
    var price = el.dataset.price,
        unit = ' ' + el.dataset.units,
        value = el.dataset.value,
        count = el.querySelector('.quantity__count'),
        txtVal = el.querySelectorAll('.quantity__value'),
        minus = el.querySelectorAll('.quantity__btn_minus'),
        plus = el.querySelectorAll('.quantity__btn_plus');

    [].slice.call(minus).forEach(function (btn, pos) {
        btn.addEventListener('click', function (e) {
            e.preventDefault();
            if (parseInt(count.value) >= 2) {
                count.value = parseInt(count.value) - 1;
            }
            if (parseInt(count.value) < 2) {
                disableMinus(minus, true);
            }
            quantityTxt(txtVal, count.value * value + unit);
        });
    });

    [].slice.call(plus).forEach(function (btn, pos) {
        btn.addEventListener('click', function (e) {
            e.preventDefault();
            count.value = parseInt(count.value) + 1;
            quantityTxt(txtVal, count.value * value + unit);
            if (parseInt(count.value) >= 2) {
                disableMinus(minus, false);
            }
        });
    });
}

function disableMinus(minus, change) {
    [].slice.call(minus).forEach(function (el, pos) {
        (change) ? el.classList.add('quantity__btn_disable') : el.classList.remove('quantity__btn_disable')
    });
}

function quantityTxt(items, val) {
    [].slice.call(items).forEach(function (el, pos) {
        el.innerHTML = val;
    });
}

function leftMenu() {
    var btn = document.querySelectorAll('.l-menu__itm_parent');
    if (btn) {
        var w = document.documentElement.clientWidth;
        if (w >= 1020) {
            [].slice.call(btn).forEach(function (el, pos) {
                if (!el.classList.contains('l-menu__itm_calc')) {


                    el.classList.add('l-menu__itm_in');
                    el.dataset.maxH = outerHeight(el.querySelector('.l-menu__sub')) + 22 + 'px';
                    el.classList.remove('l-menu__itm_in');
                    el.classList.add('l-menu__itm_calc');

                }
                el.addEventListener('mouseover', function () {
                    if (w >= 1020) {
                        this.style.maxHeight = this.dataset.maxH;
                    }

                });
                el.addEventListener('mouseout', function () {
                    if (w >= 1020) {
                        this.style.maxHeight = '22px';
                    }
                });
            });
        }
    }
}

function mainBanner() {
    var swiper = new Swiper('.bnr .swiper-container', {
        pagination: '.bnr__dots',
        autoplayDisableOnInteraction: false,
        paginationClickable: true,
        preventClicks: 'false',
        effect: 'fade',
        loop: true,
        autoplay: 2500,
    })
}

function fixedMenuMobile() {
    var w = document.documentElement.clientWidth,
        scrl = window.pageYOffset,
        menu = document.querySelector('.hdr__shop-wrp'),
        fmenu = menu.querySelector('.hdr__shop'),
        pos = menu.offsetTop;
    if (w < 1020) {
        window.addEventListener('scroll', function () {
            scrl = window.pageYOffset;
            if (scrl >= pos) {
                fmenu.classList.add('hdr__shop_fixed');
            } else {
                fmenu.classList.remove('hdr__shop_fixed');
            }
        });
    }
}

function fixedMenuDesktop() {
    var w = document.documentElement.clientWidth,
        scrl = window.pageYOffset,
        menu = document.querySelector('.t-menu__cnt'),
        fMenu = menu.querySelector('.t-menu'),
        item = menu.querySelector('.t-menu__l_in'),
        line = menu.querySelector('.t-menu__line'),
        pos = menu.offsetTop;
    if (item) {
        line.style.left = (item.offsetLeft + item.clientWidth / 2) + 'px';
    }
    if (w >= 1020) {
        window.addEventListener('scroll', function () {
            scrl = window.pageYOffset;
            if (scrl >= pos) {
                fMenu.classList.add('t-menu_fixed');
                line.classList.add('t-menu__line_in');
            } else {
                fMenu.classList.remove('t-menu_fixed');
                line.classList.remove('t-menu__line_in');
            }
        });
    }
}

function modalOrder() {
    var mdlBtn = document.querySelectorAll('.modal-btn'),
        close = document.querySelectorAll('.modal__close, .modal__close-area'),
        modal = document.querySelector('.modal');
    if (mdlBtn) {
        [].slice.call(mdlBtn).forEach(function (el, pos) {
            el.addEventListener('click', function (e) {
                e.preventDefault();
                var caption = modal.querySelector('.m-form__caption'),
                    hidden = modal.querySelector('.m-form__product-name'),
                    sub = modal.querySelector('.m-form__sub_change');
                if (el.classList.contains('modal-btn_fast')) {
                    caption.textContent = 'состав быстрого заказа';
                    sub.textContent = el.parentNode.querySelector('.goods__link').textContent;
                    hidden.value = el.parentNode.querySelector('.goods__link').textContent;
                }
                else {
                    sub.textContent = document.querySelector('.h1').textContent;
                    hidden.value = document.querySelector('.h1').textContent;
                }
                modal.classList.add('in');
            });
        });
    }
    if (close) {
        [].slice.call(close).forEach(function (el, pos) {
            el.addEventListener('click', function (e) {
                e.preventDefault();
                modal.classList.remove('in');
            });
        });
    }
}

function modalClose() {
    var close = document.querySelectorAll('.modal__close, .modal__close-area'),
        modal = document.querySelector('.modal');
    if (close) {
        [].slice.call(close).forEach(function (el, pos) {
            el.addEventListener('click', function (e) {
                e.preventDefault();
                window.location.hash = '';
                modal.classList.remove('in');
            });
        });
    }
}

function formAnswer(data) {
    var modal = document.getElementById('thankyou');
    modal.querySelector('.m-form__container').innerHTML = data;
    modal.classList.add('in');
}

function clearPlaceholder() {
    var txt = null,
        inputs = document.querySelectorAll('input, textarea');
    if (inputs) {
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].addEventListener('focus', function () {
                txt = this.placeholder;
                this.placeholder = '';
            });
            inputs[i].addEventListener('blur', function () {
                this.placeholder = txt;
            });
        }
    }
}

function burger() {
    var
        w = document.documentElement.clientWidth,
        container = document.querySelector('.hdr__shop'),
        btn = container.querySelector('.burger__btn'),
        btnC = container.querySelector('.i-bskt'),
        btnU = container.querySelector('.i-user'),
        mMenu = container.querySelector('.hdr__content'),
        cart = container.querySelector('.m-basket'),
        user = container.querySelector('.m-user'),
        menu = container.querySelector('.m-menu');

    btnC.addEventListener('click', function () {
        w = document.documentElement.clientWidth;

        /*
         Показываем контейнер
         */
        if (!mMenu.classList.contains('hdr__content_in')) {
            mMenu.classList.add('hdr__content_in');

        } else if (w >= 768) {
            mMenu.classList.remove('hdr__content_in');
        }
        /*
         Включаем бургер
         */
        if (!btn.classList.contains('in')) {
            btn.classList.add('in');
            btn.parentElement.classList.add('in');
        } else if (w >= 768) {
            btn.classList.remove('in');
            btn.parentElement.classList.remove('in');
        }

        if (w < 768) {
            /*
             Отключаем бургер и контейнер если активна кнопка корзины
             */
            if (this.classList.contains('in')) {
                btn.classList.remove('in');
                btn.parentElement.classList.remove('in');
                mMenu.classList.remove('hdr__content_in');
            }

            /*
             Отключаем меню, и все доп. классы от Авторизации
             */
            menu.classList.remove('m-menu_in');
            btnU.classList.remove('in');
            mMenu.classList.remove('hdr__content_user');
            user.classList.remove('m-user_in');

            /*
             Переключатели корзины
             */

            this.classList.toggle('in');
            mMenu.classList.toggle('hdr__content_cart');
            cart.classList.toggle('m-basket_in');
        }

    });

    btnU.addEventListener('click', function () {
        w = document.documentElement.clientWidth;
        /*
         Показываем контейнер
         */
        if (!mMenu.classList.contains('hdr__content_in')) {
            mMenu.classList.add('hdr__content_in');

        } else if (w >= 768) {
            mMenu.classList.remove('hdr__content_in');
        }
        /*
         Включаем бургер
         */
        if (!btn.classList.contains('in')) {
            btn.classList.add('in');
            btn.parentElement.classList.add('in');
        } else if (w >= 768) {
            btn.classList.remove('in');
            btn.parentElement.classList.remove('in');
        }

        if (w < 768) {
            /*
             Отключаем бургер и контейнер если активна кнопка пользователя
             */
            if (this.classList.contains('in')) {
                btn.classList.remove('in');
                btn.parentElement.classList.remove('in');
                mMenu.classList.remove('hdr__content_in');
            }

            /*
             Отключаем меню, и все доп. классы от Корзины
             */
            menu.classList.remove('m-menu_in');
            btnC.classList.remove('in');
            mMenu.classList.remove('hdr__content_cart');
            cart.classList.remove('m-basket_in');

            /*
             Переключатели Пользователя
             */

            this.classList.toggle('in');
            mMenu.classList.toggle('hdr__content_user');
            user.classList.toggle('m-user_in');
        }

    });

    btn.addEventListener('click', function () {
        /*
         Показываем блок меню
         */
        if (!menu.classList.contains('m-menu_in')) {
            menu.classList.add('m-menu_in');
        }
        /*
         Показываем контейнер
         */
        if (!mMenu.classList.contains('hdr__content_in')) {
            mMenu.classList.add('hdr__content_in');
        }
        /*
         Переключатель бургера
         */
        this.classList.toggle('in');
        this.parentElement.classList.toggle('in');

        /*
         Если бургер "выключен" прячем меню и контейнер
         */
        if (!this.classList.contains('in')) {
            mMenu.classList.remove('hdr__content_in');
            menu.classList.remove('m-menu_in');
        }
        /*
         Выключаем все остальные кнопки и элементы внутри контейнера
         */
        btnC.classList.remove('in');
        btnU.classList.remove('in');
        cart.classList.remove('m-basket_in');
        user.classList.remove('m-user_in');
        mMenu.classList.remove('hdr__content_cart', 'hdr__content_user');
    });
}

function outerHeight(el) {
    var height = el.offsetHeight;
    var style = getComputedStyle(el);
    height += parseInt(style.marginTop) + parseInt(style.marginBottom);
    return height;
}

function loadMap() {
    var map = document.querySelector('.map');
    if (map) {
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = "///api-maps.yandex.ru/2.1/?lang=ru_RU";
        script.defer = true;
        //IE triggers this event when the file is loaded

        if (script.attachEvent) {
            script.attachEvent('onreadystatechange', function () {
                if (script.readyState == 'complete' || script.readyState == 'loaded')
                    setMap();
            });
        }

        //Other browsers trigger this one
        if (script.addEventListener) script.addEventListener('load', setMap, false);

        document.body.appendChild(script);
    }
}

function setMap() {
    if (typeof ymaps !== 'undefined') {
        ymaps.ready(function () {
            ymaps.ready(init);
        });
    }
}

function init() {
    ymaps.ready(function () {
        var objct = document.querySelector('.map'),
            data = objct.dataset,
            map = new ymaps.Map(objct, {
                center: [data.lat, data.lng],
                zoom: data.zoom,
                controls: ["smallMapDefaultSet"]
            }, {
                searchControlProvider: "yandex#search"
            }),
            c = new ymaps.Placemark([data.lat, data.lng], {}, {

                /*iconLayout: 'default#image',

                 iconImageHref: '/wp-content/themes/football/assets/dist/images/mark.png',

                 iconImageSize: [44, 70],

                 iconImageOffset: [-22, -70],*/
            });
        map.behaviors.disable("scrollZoom");
        map.geoObjects.add(c);
    })
}

function loadSVG() {
    var sprire = document.createElement('div'),
        body = document.body || document.getElementsByTagName('body')[0],
        el = document.querySelector('.wrapper'),
        svg = localStorage.getItem("spriteSvg"),
        path = '/local/templates/cookery/assets/dist/svg/sprite.svg';
    if (svg) {
        sprire.innerHTML = svg;
        sprire.classList.add('sprite-svg');
        /*if (style.styleSheet) {
         style.styleSheet.cssText = css;
         } else {
         style.appendChild(document.createTextNode(css));
         }*/
    } else {
        axios.get(path)
            .then(function (response) {
                svg = response.data;
                localStorage.setItem("spriteSvg", svg)
                sprire.innerHTML = svg;
                sprire.classList.add('sprite-svg');
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    body.insertBefore(sprire, el);
}

function gallery() {
    var gallery = document.querySelector('.product__thumbnails');

    if (gallery) {
        lightGallery(gallery, {
            thumbnail: true,
        });
    }
}

function tableWrap() {
    var table = document.querySelectorAll('table');
    for (var i = 0, ln = table.length; i < ln; i++) {
        var div = document.createElement('div');
        div.setAttribute('class', 'table-wrap');
        insertAfter(div, table[i]);
        table[i].nextSibling.appendChild(table[i]);
    }
}

function insertAfter(elem, refElem) {
    return refElem.parentNode.insertBefore(elem, refElem.nextSibling);
}

function addPhoneMask($) {
    $('.phone-mask').mask("+7 (999)-999-99-99");
}

function newsAnimation() {
    [].slice.call(document.querySelectorAll('.tilter')).forEach(function (el, pos) {
        new TiltFx(el, {
            movement: {
                lines: {
                    translation: {x: 40, y: 40, z: 0},
                    reverseAnimation: {duration: 1500, easing: 'easeOutElastic'}
                },
                caption: {
                    translation: {x: 5, y: 5, z: 0},
                    rotation: {x: 0, y: 0, z: 0},
                    reverseAnimation: {duration: 1000, easing: 'easeOutExpo'}
                },
                overlay: {
                    translation: {x: -15, y: -15, z: 0},
                    rotation: {x: 0, y: 0, z: 0},
                    reverseAnimation: {duration: 750, easing: 'easeOutExpo'}
                },
                shine: {
                    translation: {x: 100, y: 100, z: 0},
                    reverseAnimation: {duration: 750, easing: 'easeOutExpo'}
                }
            }
        });
    });
}

function sendFormOrder() {
    var form = document.querySelector('.f-ordering');

    if (form) {
        form.addEventListener('submit', function (e) {
            e.preventDefault();
            var data = encodeURIComponent(toJSONString(form));
            console.log(data);
            axios.post('/', 'data=' + data)
                .then(function (response) {
                    console.log('test');
                })
                .catch(function (error) {
                    console.log(error);
                });
        });
    }

}

function toJSONString(form) {
    var obj = {};
    var elements = form.querySelectorAll("input, select, textarea");
    for (var i = 0; i < elements.length; ++i) {
        var element = elements[i];
        var name = element.name;
        var value = element.value;

        if (name) {
            obj[name] = value;
        }
    }
    return JSON.stringify(obj);
}

function loadMainCss() {
    var style = document.createElement('style'),
        head = document.head || document.getElementsByTagName('head')[0],
        css = localStorage.getItem("mainStyle"),
        path = '/local/templates/cookery/assets/dist/css/main.min.css';
    if (css) {
        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }
    } else {
        axios.get(path)
            .then(function (response) {
                css = response.data;
                localStorage.setItem("mainStyle", css)
                if (style.styleSheet) {
                    style.styleSheet.cssText = css;
                } else {
                    style.appendChild(document.createTextNode(css));
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    head.appendChild(style);
    setTimeout(initSliderAndGallery, 100);
}

function initSliderAndGallery() {
    mainBanner();
}