var mainFont = new FontFaceObserver('MVideo');

Promise.all([mainFont.load()]).then(function () {
    document.documentElement.className += " fonts-loaded";
    sessionStorage.foutFontsLoaded = true;
});