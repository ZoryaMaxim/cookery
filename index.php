<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#8eefd7">
    <link rel="icon" href="/local/templates/cookery/assets/dist/img/cropped-favicon-32x32.png" sizes="32x32"/>
    <link rel="icon" href="/local/templates/cookery/assets/dist/img/cropped-favicon-192x192.png" sizes="192x192"/>
    <link rel="apple-touch-icon-precomposed"
          href="/local/templates/cookery/assets/dist/img/cropped-favicon-180x180.png"/>
    <meta name="msapplication-TileImage"
          content="/local/templates/cookery/assets/dist/img/cropped-favicon-270x270.png"/>
    <title>Воронцовская кулинария</title>
    <meta name="robots" content="noindex,nofollow"/>
    <script>
        (function () {
            if (sessionStorage.foutFontsLoaded) {
                document.documentElement.className += " fonts-loaded";
                return;
            }
            <?php require_once dirname(__FILE__) . '/local/templates/cookery/assets/src/js/fontfaceobserver.standalone.js'; ?>
            <?php require_once dirname(__FILE__) . '/local/templates/cookery/assets/dist/js/fonts.min.js'; ?>
        })();
    </script>
    <style><?php require_once dirname(__FILE__) . '/local/templates/cookery/assets/dist/css/head.min.css'; ?></style>
    <?php $path = '/local/templates/cookery/assets/'; ?>
</head>
<body class="placeholder__body">
<div class="placeholder">
    <img class="placeholder__img" src="/local/templates/cookery/assets/dist/img/front-logo.svg" alt="logo">
    <p class="placeholder__text">Скоро можно будет заказать вкусную еду</p>
</div>
</body>
</html>