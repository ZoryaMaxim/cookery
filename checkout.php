<?php $page = 'checkout' ?>
<?php require_once 'header.php' ?>
    <div class="t-menu__cnt">
        <nav class="t-menu t-menu_inner">
            <div class="cnt">
                <ul class="t-menu__lst">
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">Меню от шефа</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">Кулинария</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l t-menu__l_in">Наша ферма</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">Пекарня/десерты</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">Овощи/Фрукты</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">Лучшее детям</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">Лидеры продаж</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">Предложение дня</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">Новинки</a></li>
                    <li class="t-menu__itm"><a href="#" class="t-menu__l">О нас</a></li>
                </ul>

            </div>
            <div class="t-menu__line"></div>
        </nav>
    </div>
    <div class="cnt">
        <h1 class="h1 h1_checkout">Оформление заказа</h1>
        <p class="checkout-title">1. Проверьте товары в Вашей корзине.</p>
        <ul class="basket">
            <li class="basket__item">
                <div class="basket__left">
                    <p class="basket__title">Молоко пастеризованное 7%</p>
                    <div class="basket__remove basket__remove_mobile"></div>
                </div>
                <div class="basket__right">
                    <div class="quantity quantity_basket">
                        <a href="#"
                           class="quantity__btn quantity__btn_minus quantity__btn_basket quantity__btn_disable">-</a>
                        <div class="quantity__value quantity__value_basket">200 гр.</div>
                        <a href="#" class="quantity__btn quantity__btn_plus quantity__btn_basket">+</a>
                        <input type="text" class="bx_col_input quantity__count" name="quantity"
                               value="1">
                    </div>
                    <div class="basket__price">379.00</div>
                    <div class="basket__remove"></div>
                </div>
            </li>
            <li class="basket__item">
                <div class="basket__left">
                    <p class="basket__title">Молоко пастеризованное 7%</p>
                    <div class="basket__remove basket__remove_mobile"></div>
                </div>
                <div class="basket__right">
                    <div class="quantity quantity_basket">
                        <a href="#"
                           class="quantity__btn quantity__btn_minus quantity__btn_basket quantity__btn_disable">-</a>
                        <div class="quantity__value quantity__value_basket">200 гр.</div>
                        <a href="#" class="quantity__btn quantity__btn_plus quantity__btn_basket">+</a>
                        <input type="text" class="bx_col_input quantity__count" name="quantity"
                               value="1">
                    </div>
                    <div class="basket__price">379.00</div>
                    <div class="basket__remove"></div>
                </div>
            </li>
            <li class="basket__item">
                <div class="basket__left">
                    <p class="basket__title">Молоко пастеризованное 7%</p>
                    <div class="basket__remove basket__remove_mobile"></div>
                </div>
                <div class="basket__right">
                    <div class="quantity quantity_basket">
                        <a href="#"
                           class="quantity__btn quantity__btn_minus quantity__btn_basket quantity__btn_disable">-</a>
                        <div class="quantity__value quantity__value_basket">200 гр.</div>
                        <a href="#" class="quantity__btn quantity__btn_plus quantity__btn_basket">+</a>
                        <input type="text" class="bx_col_input quantity__count" name="quantity"
                               value="1">
                    </div>
                    <div class="basket__price">379.00</div>
                    <div class="basket__remove"></div>
                </div>
            </li>
        </ul>
        <p class="basket-total">В Вашей корзине <span class="basket-total__count">3</span> товара <br>на общую сумму
            <span class="basket-total__price">1137</span>.
        </p>
        <div class="basket-money-box">
            <?php if (true): /*Неавторизирован*/ ?>
                <p class="basket-money-box__text">Для использования копилки <br>Вам необходимо авторизоваться.</p>
                <div class="basket-money-box__btn">Ваша копилка</div>
            <?php endif; ?>
            <?php if (false): /*авторизирован*/ ?>
                <p class="basket-money-box__text">В Вашей копилке: <span class="basket-money-box__price">590</span>.</p>
                <div class="basket-money-box__btn">Потратить</div>
            <?php endif; ?>
        </div>
        <div class="checkout-auth">
            <p class="checkout-auth__cnt">
                <?php if (true): /*Неавторизован*/ ?>
                    <span class="checkout-auth__txt">2. Для оформления заказа <br>Вам необходимо</span><span
                            class="checkout-auth__btn">Войти</span><span class="checkout-auth__txt">или</span>
                    <span class="checkout-auth__btn checkout-auth__btn_reg">Зарегистрироваться</span>
                <?php endif; ?>
                <?php if (false):/* Авторизован*/ ?>
                    <span class="checkout-auth__online">2. Вы авторизованы:</span>
                    <span class="checkout-auth__name">Алексей</span>
                    <span class="checkout-auth__phone">+7 901 902 33 44</span>
                    <span class="checkout-auth__change">Сменить пароль</span>
                <?php endif; ?>
            </p>
        </div>
        <p class="checkout-title">3. Введите адрес доставки или найдите место на карте</p>
        <div class="delivery">
            <ul class="delivery__list">
                <li class="delivery__item">
                    <input type="radio" checked name="delivery" id="delivery-1" class="delivery__radio">
                    <label for="delivery-1" class="delivery__cnt">
                        <span class="delivery__title delivery__title_adress">Москва, ул. Кравченко, 4к1, кв. 121, Вход через дверь справа </span>
                        <span class="delivery__change">Изменить</span>
                        <span class="delivery__change">Удалить</span>
                    </label>
                </li>
                <li class="delivery__item">
                    <input type="radio" disabled name="delivery" id="delivery-2" class="delivery__radio">
                    <label for="delivery-2" class="delivery__cnt">
                        <span class="delivery__title">Скоро! Самовызов: Ленинский, 101</span>
                    </label>
                </li>
                <li class="delivery__item">
                    <input type="radio" name="delivery" id="delivery-form" class="delivery__radio">
                    <label for="delivery-form" class="delivery__cnt">
                        <span class="delivery__title">Добавить адрес</span>
                    </label>
                </li>
            </ul>
        </div>
        <form action="?" method="post" class="delivery-form">
            <div class="delivery-form__data">
                <div class="delivery-form__row">
                    <div class="delivery-form__wrp">
                        <input type="text" name="deliveryPoint[Locality]" placeholder="Населенный пункт"
                               class="delivery-form__input delivery-form__input_locality">
                    </div>
                    <div class="delivery-form__wrp">
                        <input type="text" name="deliveryPoint[street]" placeholder="Улица / Переулок"
                               class="delivery-form__input delivery-form__input_street">
                    </div>
                </div>
                <div class="delivery-form__row">
                    <div class="delivery-form__wrp">
                        <input type="text" name="deliveryPoint[house]" placeholder="Дом"
                               class="delivery-form__input delivery-form__input_house">
                    </div>
                    <div class="delivery-form__wrp">
                        <input type="text" name="deliveryPoint[flat]" placeholder="Квартира"
                               class="delivery-form__input delivery-form__input_flat">
                    </div>
                </div>
            </div>
            <div class="delivery-form__comments">
                <div class="delivery-form__wrp delivery-form__wrp_comment">
                    <textarea name="deliveryPoint[comment]" placeholder="Комментарий"
                              class="delivery-form__input delivery-form__input_area"></textarea>
                </div>
                <div class="delivery-form__wrp delivery-form__wrp_status">
                    <div class="delivery-form__status "></div><!--delivery-form__status_ok-->
                </div>
                <div class="delivery-form__wrp delivery-form__wrp_submit">
                    <button class="delivery-form__submit" disabled>Добавить</button>
                </div>
            </div>
        </form>
        <div class="map" data-lat="55.6667" data-lng="37.515471" data-zoom="11"
             data-address="Ленинский проспект, 101, Москва, город Москва, Россия"></div>
        <ul class="delivery-dist">
            <li class="delivery-dist__item"></li>
            <li class="delivery-dist__item"></li>
            <li class="delivery-dist__item"></li>
        </ul>
        <p class="checkout-title checkout-title_payment">4. Выберите способ оплаты</p>
        <ul class="payments">
            <li class="payments__item">
                <input checked type="radio" name="payments" id="payments-1" class="payments__radio">
                <label for="payments-1" class="payments__wrp">
                    <span class="payments__icon payments__icon_cash"></span>
                    <span class="payments__title payments__title_withdrawal">наличными<br>курьеру</span>
                    <span class="payments__withdrawal">Сдача с <input type="text" name="withdrawal"
                                                                      class="payments__withdrawal-input"></span>
                </label>
            </li>
            <li class="payments__item">
                <input type="radio" name="payments" id="payments-2" class="payments__radio">
                <label for="payments-2" class="payments__wrp">
                    <span class="payments__icon payments__icon_cur"></span>
                    <span class="payments__title">Картой<br>курьеру</span>
                </label>
            </li>
            <li class="payments__item">

                <input type="radio" name="payments" id="payments-3" class="payments__radio">
                <label for="payments-3" class="payments__wrp">
                    <span class="payments__icon payments__icon_card"></span>
                    <span class="payments__title">Картой<br>онлайн</span>
                </label>
            </li>
        </ul>
        <div class="checkout-total">
            <p class="checkout-total__title">Итого к оплате:</p>
            <p class="checkout-total__price">1337¤</p>
            <p class="checkout-total__btn">Оформить</p>
        </div>
    </div>


<?php require_once 'footer.php' ?>